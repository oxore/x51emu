# MCS51 emulator

## TODO for MVP of MCS51 emulator

- Fully implements ISA of Atmel 8051.
- Seek to implement compatibility with AT89S52.
- Internal Flash ROM (8K to 64K).
- Internal RAM (256 byte).
- External ROM (up to 64K).
- Interrupts support.
- Peripherals:
  - At least one I/O Port (P0)
  - Timer (with interrupts)
  - Serial UART (with interrupts)
- Cycle accurate emulation of program execution and synchronization with all
  peripherals.
- Locking execution speed to real host machine time with speed multiplier (e.g.
  1 instruction per second, 1000 cycles per second).
- Every peripheral produces event with information of it's state if it changes.
  CPU produces events on each step while in single step mode or when pause
  occurs.
- Emulator consumes control events for changing peripherals state, controlling
  execution speed or stepping CPU.
- CLI controller.

## TODO Beyond MVP

- Full compatibility with AT89S52.
- CPU step back command and reverse execution for some amount of steps (from few
  to billions).
- TUI controller.
- Comprehensive GUI in web-browser.
