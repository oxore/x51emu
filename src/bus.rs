extern crate num_traits;

pub trait Bus<Addr>
where
    Addr: Copy + core::ops::Add<Output = Addr> + num_traits::identities::One,
{
    fn get(&self, a: Addr) -> u8;

    fn set(&mut self, a: Addr, v: u8);

    fn get_word(&self, a: Addr) -> u16 {
        u16::from(self.get(a)) | (u16::from(self.get(a + Addr::one())) << 8)
    }

    fn set_word(&mut self, a: Addr, v: u16) {
        self.set(a, (v & 0xFF) as u8);
        self.set(a + Addr::one(), (v >> 8) as u8)
    }
}
