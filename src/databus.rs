use super::bus::Bus;
use super::memdata::MemoryData;
use super::peripheral::MappedPeripheral;
use super::ram::Ram;

const RESET_VALUE_ACC: u8 = 0x00;
const RESET_VALUE_SP: u8 = 0x07;

pub const DBUS_OFFSET_ACC: u8 = 0xE0;
pub const DBUS_OFFSET_SP: u8 = 0x80;
pub const DBUS_OFFSET_DPH: u8 = 0x82;
pub const DBUS_OFFSET_DPL: u8 = 0x83;
pub const DBUS_OFFSET_R0: u8 = 0x00;
pub const DBUS_OFFSET_R1: u8 = 0x01;
pub const DBUS_OFFSET_R2: u8 = 0x02;
pub const DBUS_OFFSET_R3: u8 = 0x03;
pub const DBUS_OFFSET_R4: u8 = 0x04;
pub const DBUS_OFFSET_R5: u8 = 0x05;
pub const DBUS_OFFSET_R6: u8 = 0x06;
pub const DBUS_OFFSET_R7: u8 = 0x07;
pub const DBUS_OFFSET_P0: u8 = 0x80;
pub const DBUS_OFFSET_P1: u8 = 0x90;

pub struct DataBus {
    ram: Ram,
    acc: u8,
    sp: u8,
    periph: Vec<MappedPeripheral>,
}

impl DataBus {
    pub fn with_ram_data<A: Into<MemoryData>>(mut self, data: A) -> Self {
        self.load_ram_data(data);
        self
    }

    /// Method for occasional incremental data loading
    pub fn load_ram_data<A: Into<MemoryData>>(&mut self, data: A) {
        self.ram.load_data(data);
    }
}

impl Bus<u8> for DataBus {
    fn get(&self, a: u8) -> u8 {
        match a {
            0..=0x7F => self.ram.get(a),
            DBUS_OFFSET_ACC => self.acc,
            DBUS_OFFSET_SP => self.sp,
            _ => self.periph.iter().fold(0, |acc, x| acc | x.borrow().get(a)),
        }
    }

    fn set(&mut self, a: u8, v: u8) {
        match a {
            0..=0x7F => self.ram.set(a, v),
            DBUS_OFFSET_ACC => self.acc = v,
            DBUS_OFFSET_SP => self.sp = v,
            _ => self
                .periph
                .iter_mut()
                .for_each(|x| x.borrow_mut().set(a, v)),
        }
    }
}

impl Default for DataBus {
    fn default() -> Self {
        Self {
            ram: Ram::default(),
            acc: RESET_VALUE_ACC,
            sp: RESET_VALUE_SP,
            periph: Vec::default(),
        }
    }
}
