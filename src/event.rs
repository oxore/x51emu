pub type Ticks = u32;

pub enum EventIO {
    LogicLevel(u8),
    InputConfig(u8),
}

pub enum EventType {
    IO(u8),
}

pub struct Event {
    pub periph_name: String,
    pub time: Ticks,
    pub data: EventType,
}
