use super::bus::Bus;
use super::event::Event;
use super::peripheral::EventCapable;

pub struct IOPort {
    name: String,
    addr: u8,
    bits: u8,
}

pub struct IOPortBuilder {
    name: String,
    addr: u8,
}

impl Default for IOPortBuilder {
    fn default() -> Self {
        Self {
            name: String::from("p0"),
            addr: 0u8,
        }
    }
}

impl IOPortBuilder {
    pub fn map_addr(mut self, a: u8) -> Self {
        self.addr = a;
        self
    }
    pub fn with_name(mut self, name: &str) -> Self {
        self.name = String::from(name);
        self
    }
    pub fn finish(&self) -> IOPort {
        IOPort {
            name: self.name.clone(),
            addr: self.addr,
            bits: 0,
        }
    }
}

impl Bus<u8> for IOPort {
    fn get(&self, a: u8) -> u8 {
        if a == self.addr {
            return self.bits;
        }
        0
    }

    fn set(&mut self, a: u8, v: u8) {
        if a == self.addr {
            self.bits = v;
        }
    }
}

impl EventCapable for IOPort {
    fn name(&self) -> &str {
        self.name.as_ref()
    }
    fn pump(&mut self) -> Option<Box<Event>> {
        None
    }
    fn push(&mut self, _e: Box<Event>) {
        // TODO: Implement
    }
}
