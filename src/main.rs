extern crate clap;

use std::cell::RefCell;
use std::fs;
use std::io;
use std::io::prelude::*;
use std::rc::Rc;
use std::sync::mpsc::{channel, Receiver, Sender};
use std::thread;
use std::time::Duration;

pub mod bus;
pub mod cpu8051;
pub mod databus;
pub mod event;
pub mod ioport;
pub mod memdata;
pub mod peripheral;
pub mod progmem;
pub mod ram;

use self::cpu8051::{Cpu8051, Cpu8051Builder};
use self::databus::DBUS_OFFSET_P0;
use self::databus::DBUS_OFFSET_P1;
use self::ioport::IOPortBuilder;
use self::memdata::MemoryData;
use self::peripheral::EventCapable;

enum Control {
    Run(bool),
}

enum SimData {}

struct BoundController {
    tx: Sender<Control>,
    _rx: Receiver<SimData>,
}

struct BoundEmu {
    _tx: Sender<SimData>,
    rx: Receiver<Control>,
}

fn new_bound_pair() -> (BoundEmu, BoundController) {
    let (tx_c, rx_c): (Sender<Control>, Receiver<Control>) = channel();
    let (tx_d, rx_d): (Sender<SimData>, Receiver<SimData>) = channel();
    (
        BoundEmu {
            _tx: tx_d,
            rx: rx_c,
        },
        BoundController {
            tx: tx_c,
            _rx: rx_d,
        },
    )
}

fn cli_controller(bound: BoundController) {
    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        let line = line.unwrap();
        match line.as_ref() {
            "stop" => {
                println!("stop");
                bound.tx.send(Control::Run(false)).unwrap();
            }
            "start" => {
                println!("start");
                bound.tx.send(Control::Run(true)).unwrap();
            }
            _ => (),
        }
    }
}

fn ws_controller(_bound: BoundController) {
    // TODO implement
    println!("AAA");
}

fn new_controller(name: &str) -> Result<&dyn Fn(BoundController), i32> {
    match name {
        "cli" => Ok(&cli_controller),
        "ws" => Ok(&ws_controller),
        _ => Err(1),
    }
}

fn emu_worker(mut cpu: Cpu8051, bound: BoundEmu) {
    let mut should_stop = false;
    loop {
        if should_stop {
            if let Ok(msg) = bound.rx.recv() {
                match msg {
                    Control::Run(run) => should_stop = !run,
                }
            }
        } else {
            println!("0x{:08x}: {}", cpu.pc(), cpu.op());
            cpu.step();
            if let Ok(msg) = bound.rx.recv_timeout(Duration::from_millis(1000)) {
                match msg {
                    Control::Run(run) => should_stop = !run,
                }
            }
        }
    }
}

fn load_from_bin(filename: &str, offset: usize) -> MemoryData {
    let data = fs::read(filename).unwrap_or_else(|_| panic!("Unable to read file {}", filename));
    MemoryData::from_bin(offset, &data)
}

fn load_from_hex(filename: &str) -> MemoryData {
    let data =
        fs::read_to_string(filename).unwrap_or_else(|_| panic!("Unable to read file {}", filename));
    MemoryData::from_hex(data).unwrap()
}

fn main() {
    /* Parse args */

    let cli_args_matches = clap::App::new("x51emu")
        .version("0.1.0")
        .author("oxore")
        .about("MCS51 emulator")
        .arg(
            clap::Arg::with_name("file")
                .short("f")
                .long("file")
                .value_name("FILE[,offset]")
                .use_delimiter(true)
                .help(
                    "Sets a file with program to load; if `offset` provided then it assumes `bin`",
                )
                .takes_value(true)
                .required(true),
        )
        .arg(
            clap::Arg::with_name("interface")
                .short("i")
                .long("interface")
                .value_name("INTERFACE")
                .help("Sets a controlling and I/O interface")
                .takes_value(true)
                .default_value("cli")
                .possible_values(&["cli", "ws"]),
        )
        .get_matches();

    let controller_type_name = String::from(
        cli_args_matches
            .value_of("interface")
            .expect("No interface type specified"),
    );

    let mut file_arg = cli_args_matches
        .values_of("file")
        .expect("No file name provided")
        .collect::<Vec<_>>();

    /* Read program data */

    let progmem_filename = file_arg.remove(0);
    let progmem_data = if file_arg.len() >= 2 {
        let offset: usize = file_arg.remove(0).parse().unwrap();
        load_from_bin(progmem_filename, offset)
    } else {
        load_from_hex(progmem_filename)
    };

    /* Instantiate an emulator */

    let p0 = Rc::new(RefCell::new(
        IOPortBuilder::default()
            .map_addr(DBUS_OFFSET_P0)
            .with_name("p0")
            .finish(),
    ));
    let p1 = Rc::new(RefCell::new(
        IOPortBuilder::default()
            .map_addr(DBUS_OFFSET_P1)
            .with_name("p1")
            .finish(),
    ));

    let cpu = Cpu8051Builder::default()
        .progmem(progmem_data)
        .map_peripheral(p0.clone())
        .map_peripheral(p1.clone())
        .event_capable_periph(p0.borrow().name(), p0.clone())
        .event_capable_periph(p1.borrow().name(), p1.clone())
        .finish();

    /* Run emulation worker and controller */

    let (bound_core, bound_controller) = new_bound_pair();

    emu_worker(cpu, bound_core);

    thread::spawn(move || {
        let controller = {
            new_controller(&controller_type_name)
                .unwrap_or_else(|_| panic!("Unknown controller type \"{}\"", controller_type_name))
        };

        controller(bound_controller);
    });
}
