use std::error::Error;
use std::fmt;

enum RecordType {
    Data,
    EndOfFile,
}

struct HexLine {
    rectyp: RecordType,
    offset: u16,
    data: Vec<u8>,
}

// TODO: Rewrite with decent parsing and verbose error return types
impl HexLine {
    fn from(s: &str) -> Result<Self, usize> {
        // The shortest possible sequence is EOF (:00000001FF)
        if s.len() < 11 {
            return Err(usize::max_value());
        }
        if &s[0..1] != ":" {
            return Err(0);
        }
        let offset = match u16::from_str_radix(&s[3..7], 16) {
            Ok(value) => value,
            Err(_) => return Err(3),
        };
        let bytecount = match usize::from_str_radix(&s[1..3], 16) {
            Ok(value) => value,
            Err(_) => return Err(1),
        };

        // If EOF reached
        if &s[7..9] == "01" {
            return Ok(HexLine {
                rectyp: RecordType::EndOfFile,
                offset,
                data: vec![0],
            });
        } else if &s[7..9] == "00" {
            let mut counter = 9;
            let mut data = vec![];
            while counter < s.len() - 2 && counter < (9 + bytecount * 2) {
                data.push(match u8::from_str_radix(&s[counter..counter + 2], 16) {
                    Ok(value) => value,
                    Err(_) => return Err(counter),
                });
                counter += 2;
            }
            // TODO: check checksum
            return Ok(HexLine {
                rectyp: RecordType::Data,
                offset,
                data,
            });
        }

        Err(3)
    }
}

#[derive(Debug)]
pub struct MemoryDataHexParseError {
    // TODO: Implement after rewriting hex parsing
}

impl fmt::Display for MemoryDataHexParseError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "MemoryDataError is not fully implemented!")
    }
}

#[derive(Debug)]
pub enum MemoryDataError {
    HexParse(MemoryDataHexParseError),
}

impl fmt::Display for MemoryDataError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "HexParse: {}",
            match self {
                MemoryDataError::HexParse(parse_error) => parse_error,
            }
        )
    }
}

impl Error for MemoryDataError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        Some(match self {
            MemoryDataError::HexParse(parse_error) => parse_error,
        })
    }
}

impl Error for MemoryDataHexParseError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        None
    }
}

#[derive(Clone)]
pub struct MemoryDataChunk {
    pub offset: usize,
    pub data: Vec<u8>,
}

#[derive(Clone)]
pub struct MemoryData {
    pub chunks: Vec<MemoryDataChunk>,
}

impl MemoryData {
    pub fn from_bin(offset: usize, data: &[u8]) -> Self {
        Self {
            chunks: vec![MemoryDataChunk {
                offset,
                data: Vec::from(data),
            }],
        }
    }

    pub fn from_hex(hex: String) -> Result<Self, Box<dyn Error>> {
        let mut chunks: Vec<MemoryDataChunk> = Vec::new();
        for (_, line) in hex.lines().enumerate() {
            let hex_line = match HexLine::from(line) {
                Ok(value) => value,
                Err(_) => {
                    return Err(Box::new(MemoryDataError::HexParse(
                        MemoryDataHexParseError {},
                    )))
                }
            };
            match hex_line.rectyp {
                RecordType::Data => chunks.push(MemoryDataChunk {
                    offset: hex_line.offset as usize,
                    data: hex_line.data,
                }),
                RecordType::EndOfFile => {}
            }
        }
        Ok(MemoryData { chunks })
    }
}
