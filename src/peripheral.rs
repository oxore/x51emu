use super::bus::Bus;
use super::event::Event;

use std::cell::RefCell;
use std::rc::Rc;

pub type MappedPeripheral = Rc<RefCell<dyn Bus<u8>>>;
pub type EventCapablePeripheral = Rc<RefCell<dyn EventCapable>>;

pub trait EventCapable {
    fn name(&self) -> &str;
    fn pump(&mut self) -> Option<Box<Event>>;
    fn push(&mut self, e: Box<Event>);
}
