use super::bus::Bus;
use super::memdata::MemoryData;
use std::fmt;

pub struct Ram {
    array: [u8; 0x80],
}

impl fmt::Debug for Ram {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut v: Vec<char> = vec![];
        for (c, x) in self.array.iter().enumerate() {
            for hex_char in format!("{:02X}", *x).chars() {
                v.push(hex_char);
            }
            v.push(' ');
            if c % 8 == 7 {
                v.push('\n');
            }
        }
        write!(f, "{}", v.iter().collect::<String>())
    }
}

impl Bus<u8> for Ram {
    fn get(&self, a: u8) -> u8 {
        self.array[a as usize]
    }

    fn set(&mut self, a: u8, v: u8) {
        self.array[a as usize] = v;
    }
}

impl Default for Ram {
    fn default() -> Self {
        let array = [0; 0x80];
        Self { array }
    }
}

impl Ram {
    pub fn with_data<A: Into<MemoryData>>(mut self, data: A) -> Self {
        self.load_data(data);
        self
    }

    /// Method for occasional incremental data loading
    pub fn load_data<A: Into<MemoryData>>(&mut self, data: A) {
        for chunk in &data.into().chunks {
            if chunk.offset < self.array.len() {
                (&mut self.array[chunk.offset..])
                    .iter_mut()
                    .zip(&chunk.data)
                    .for_each(|(x, y)| *x = *y);
            }
        }
    }
}
