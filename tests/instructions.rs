use x51emu::cpu8051::Cpu8051Builder;
use x51emu::memdata::MemoryData;

#[test]
fn nop() {
    // nop
    let program = [0];
    let mut cpu = Cpu8051Builder::default()
        .progmem(MemoryData::from_bin(0, &program))
        .finish();
    cpu.step();
    assert!(cpu.pc() == 1);
}

#[test]
fn ajmp_7ffh() {
    // ajmp 7ffh
    let program = [0xE1, 0xFF];
    let mut cpu = Cpu8051Builder::default()
        .progmem(MemoryData::from_bin(0, &program))
        .finish();
    cpu.step();
    assert!(cpu.pc() == 0x7FF);
}

#[test]
fn ajmp_6ffh() {
    // ajmp 6ffh
    let program = [0xC1, 0xFF];
    let mut cpu = Cpu8051Builder::default()
        .progmem(MemoryData::from_bin(0, &program))
        .finish();
    cpu.step();
    assert!(cpu.pc() == 0x6FF);
}

#[test]
fn ajmp_5ffh() {
    // ajmp 5ffh
    let program = [0xA1, 0xFF];
    let mut cpu = Cpu8051Builder::default()
        .progmem(MemoryData::from_bin(0, &program))
        .finish();
    cpu.step();
    assert!(cpu.pc() == 0x5FF);
}

#[test]
fn ajmp_4ffh() {
    // ajmp 4ffh
    let program = [0x81, 0xFF];
    let mut cpu = Cpu8051Builder::default()
        .progmem(MemoryData::from_bin(0, &program))
        .finish();
    cpu.step();
    assert!(cpu.pc() == 0x4FF);
}

#[test]
fn ajmp_3ffh() {
    // ajmp 3ffh
    let program = [0x61, 0xFF];
    let mut cpu = Cpu8051Builder::default()
        .progmem(MemoryData::from_bin(0, &program))
        .finish();
    cpu.step();
    assert!(cpu.pc() == 0x3FF);
}

#[test]
fn ajmp_2ffh() {
    // ajmp 2ffh
    let program = [0x41, 0xFF];
    let mut cpu = Cpu8051Builder::default()
        .progmem(MemoryData::from_bin(0, &program))
        .finish();
    cpu.step();
    assert!(cpu.pc() == 0x2FF);
}

#[test]
fn ajmp_1ffh() {
    // ajmp 1ffh
    let program = [0x21, 0xFF];
    let mut cpu = Cpu8051Builder::default()
        .progmem(MemoryData::from_bin(0, &program))
        .finish();
    cpu.step();
    assert!(cpu.pc() == 0x1FF);
}

#[test]
fn ajmp_0ffh() {
    // ajmp 0ffh
    let program = [0x01, 0xFF];
    let mut cpu = Cpu8051Builder::default()
        .progmem(MemoryData::from_bin(0, &program))
        .finish();
    cpu.step();
    assert!(cpu.pc() == 0xFF);
}

#[test]
fn ajmp_0() {
    // ajmp 0
    let program = [0x01, 0x00];
    let mut cpu = Cpu8051Builder::default()
        .progmem(MemoryData::from_bin(0, &program))
        .finish();
    cpu.step();
    assert!(cpu.pc() == 0);
}

#[test]
fn ajmp_2() {
    // ajmp 2
    let program = [0x01, 0x02];
    let mut cpu = Cpu8051Builder::default()
        .progmem(MemoryData::from_bin(0, &program))
        .finish();
    cpu.step();
    assert!(cpu.pc() == 2);
}

#[test]
fn mov_a_0ffh() {
    // mov a, #0ffh
    let program = [0x74, 0xFF];
    let mut cpu = Cpu8051Builder::default()
        .progmem(MemoryData::from_bin(0, &program))
        .finish();
    cpu.step();
    assert!(cpu.dbus().borrow().get(0xE0) == 0xFF);
}

#[test]
fn mov_a_2() {
    // mov a, #2
    let program = [0x74, 0x02];
    let mut cpu = Cpu8051Builder::default()
        .progmem(MemoryData::from_bin(0, &program))
        .finish();
    cpu.step();
    assert!(cpu.dbus().borrow().get(0xE0) == 0x02);
}

#[test]
fn push_0() {
    // push 0
    let program = [0xC0, 0x00];
    let data = [0x88];
    let mut cpu = Cpu8051Builder::default()
        .progmem(MemoryData::from_bin(0, &program))
        .ram(MemoryData::from_bin(0, &data))
        .finish();
    assert!(cpu.dbus().borrow().get(0x80) == 0x07);
    cpu.step();
    assert!(cpu.dbus().borrow().get(0x80) == 0x08);
    assert!(cpu.dbus().borrow().get(0x08) == 0x88);
}

#[test]
fn pop_3() {
    // pop 3
    let program = [0xD0, 0x03];
    let data = [0x88];
    let mut cpu = Cpu8051Builder::default()
        .progmem(MemoryData::from_bin(0, &program))
        .ram(MemoryData::from_bin(7, &data))
        .finish();
    assert!(cpu.dbus().borrow().get(0x80) == 0x07);
    cpu.step();
    assert!(cpu.dbus().borrow().get(0x80) == 0x06);
    assert!(cpu.dbus().borrow().get(0x03) == 0x88);
}

#[test]
fn sjmp_42() {
    // sjmp 42
    let program = [0x80, 42];
    let mut cpu = Cpu8051Builder::default()
        .progmem(MemoryData::from_bin(0, &program))
        .finish();
    let pc_prev = cpu.pc();
    cpu.step();
    assert!(cpu.pc() == pc_prev + 2 + 42);
}
